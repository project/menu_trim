README file for the Menu Trim Drupal module.


Description
***********

The Menu Trim module allows menu hierarchies to be trimmed when navigated. By
trimming, here we mean skipping the display of parent menu items when any of
some designated items becomes active.

At the simplest level, this module allows to hide a menu until some of its items
has become the active item (by reaching the corresponding url). This is easier
than configuring the menu's block visibility for each possible path contained in
the menu.

This module can be used to make a "contextual secondary menu" based on the
primary links (e.g. you set the Primary links menu for "Allow trimming for this
menu, hide menu when no item is active", activate the "Primary links
(Menu trim)" block, then edit the menu's top-level items and check the "Trim
parent items" option). After that, when an item is selected in the primary
links, the contextual menu will appear, showing only the subitems of the
selected item. Drupal already allows contextual "secondary links" based on the
primary links, but these secondary links are limited to a depth of one
level. Menu Trim, on the other hand, can display complete menu subtrees.

This module can also be used in any menu to trim parents when reaching deep menu
items. This can make deep menus more usable, and the breadcrumb will still show
the full path.


Installation
************

1. Extract the 'menu_trim' module directory into your Drupal modules directory.

2. Enable the Menu Trim module on your site's Administer > Site building >
   Modules page.

3. Go to Administer > Site configuration > Menu trim, and configure some menus
   to allow trimming. Select "Allow trimming, hide when inactive" if you not
   only want a menu to be trimmed on some designated items, but to also make it
   "contextual" (appearing only when the url is matching some of its items is
   requested).

4. Go to Administer > Site building > Blocks, and enable the "Menu trim" blocks
   for each of the menus for which trimming was allowed.

5. Go to Administer > Site building > Menus, and edit each item that needs its
   parents to be trimmed by checking "Trim parent items" in the item's edit
   form.


Credits
*******

Initially developed on behalf of Koumbit.org (http://koumbit.org).
